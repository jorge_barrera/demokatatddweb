# coding=utf-8
from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
import random
import string


class FunctionalTest(TestCase):
    TestCase.textoAleatorio = ''.join(random.sample((string.ascii_uppercase + string.digits), 10))

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get("http://localhost:8000/")
        self.assertIn("Busco Ayuda TDD", self.browser.title)

    def test_register(self):
        self.browser.get("http://localhost:8000/register/")

        nombre = self.browser.find_element_by_id("nombre")
        nombre.send_keys(self.textoAleatorio)

        apellidos = self.browser.find_element_by_id("apellidos")
        apellidos.send_keys(self.textoAleatorio)

        self.browser.find_element_by_xpath("//select[@id='idServicio']/option[text()='Pintura']").click()

        aniosExperiencia = self.browser.find_element_by_id("aniosExperiencia")
        aniosExperiencia.send_keys("2")

        telefono = self.browser.find_element_by_id("telefono")
        telefono.send_keys("768669")

        correoElectronicoAleatorio = self.textoAleatorio + "@test.com"
        correoElectronico = self.browser.find_element_by_id("correoElectronico")
        correoElectronico.send_keys(correoElectronicoAleatorio)

        contrasenia = self.browser.find_element_by_id("contrasenia")
        contrasenia.send_keys("test")

        foto = self.browser.find_element_by_id("foto")
        foto.send_keys("C:\Users\Jorge\Desktop\descarga.jpg")

        self.browser.find_element_by_id("registrar").click()

        wait = WebDriverWait(self.browser, 10)
        alert = wait.until(EC.alert_is_present())
        result = alert.text
        alert.accept()

        self.assertIn("satisfactoriamente", result)

    def test_viewDetail(self):
        self.browser.get("http://localhost:8000/")
        self.browser.find_element_by_xpath(
            "//a[p/text()='" + self.textoAleatorio + " " + self.textoAleatorio + "']").click()

        wait = WebDriverWait(self.browser, 10)
        id_nombre = wait.until(EC.presence_of_element_located((By.ID, 'id_nombre')))

        self.assertIn(self.textoAleatorio, id_nombre.get_attribute('value'))
